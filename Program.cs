﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            string firstName = "Grace";
            string middleName = "Vania";
            string lastName = "Ocampo";

            Console.WriteLine($"Hello {firstName} {middleName} {lastName}");
            Console.WriteLine(Logger.GetDateTime());
            Console.WriteLine($"The name {firstName} has {firstName.Length} letters");
        }
    }
}
